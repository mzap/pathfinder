package pl.mzap;

import pl.mzap.astart.AStarPathSnapshot;

import java.util.Collection;

public interface PathFinding {

    Collection<AStarPathSnapshot> findPath();

}
