package pl.mzap.model;

public enum CellType {
    DEFAULT,
    OBSTACLE,
    START,
    PATH,
    OPEN,
    CLOSED,
    END;
}
