package pl.mzap.model;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.text.DecimalFormat;

public class Cell extends StackPane {

    int row;
    int column;
    boolean isObstacle;
    CellType cellType;
    Label gCost;
    Label hCost;
    Label fCost;

    public Cell(int row, int column, double cellXSize, double cellYSize, CellType cellType) {
        this(row, column, cellXSize, cellYSize);
        this.cellType = cellType;
        setCellStyle();
    }

    public Cell(int row, int column, double cellXSize, double cellYSize) {
        this.row = row;
        this.column = column;
        this.isObstacle = false;
        this.cellType = CellType.DEFAULT;

        setPrefWidth(cellXSize);
        setPrefHeight(cellYSize);
        setLayoutX(cellXSize * column);
        setLayoutY(cellYSize * row);

        gCost = new Label("0");
        hCost = new Label("0");
        fCost = new Label("0");
        hideLabels();
        setLabelStyle();

        HBox horizontalBox = new HBox();
        horizontalBox.setSpacing(10);
        horizontalBox.setAlignment(Pos.CENTER);

        horizontalBox.getChildren().addAll(gCost, hCost);

        VBox verticalBox = new VBox();
        verticalBox.setSpacing(5);
        verticalBox.setAlignment(Pos.CENTER);
        verticalBox.getChildren().addAll(horizontalBox, fCost);

        setCellStyle();
        getChildren().addAll(verticalBox);
    }

    public void setLabelStyle() {
        gCost.getStyleClass().add("label-g");
        fCost.getStyleClass().add("label-f");
        hCost.getStyleClass().add("label-h");
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
        setCellStyle();
    }

    public void setCellLabels(double gCost, double fCost, double hCost) {
        this.gCost.setVisible(true);
        this.fCost.setVisible(true);
        this.hCost.setVisible(true);
        DecimalFormat decimalFormat = new DecimalFormat("###.#");

        this.gCost.setText(decimalFormat.format(gCost));
        this.hCost.setText(decimalFormat.format(hCost));
        this.fCost.setText(decimalFormat.format(fCost));
    }

    public void markAsObstacle() {
        this.isObstacle = true;
        this.cellType = CellType.OBSTACLE;
        setCellStyle();
        hideLabels();
    }

    public void removeObstacle() {
        this.isObstacle = false;
        this.cellType = CellType.DEFAULT;
        setCellStyle();
        hideLabels();
    }

    void setCellStyle() {
        getStyleClass().clear();
        switch (cellType) {
            case DEFAULT:
                getStyleClass().add("cell");
                break;
            case OBSTACLE:
                getStyleClass().add("obstacle");
                break;
            case PATH:
                getStyleClass().add("path");
                break;
            case OPEN:
                getStyleClass().add("open");
                break;
            case CLOSED:
                getStyleClass().add("closed");
                break;
            case START:
                getStyleClass().add("start");
                break;
            case END:
                getStyleClass().add("end");
                break;
        }
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isObstacle() {
        return isObstacle;
    }

    public CellType getCellType() {
        return cellType;
    }

    @Override
    public String toString() {
        return "Cell[" + row + "][" + column + "]";
    }

    public void setPosition(int row, int col) {
        this.row = row;
        this.column = col;
    }

    public void clear() {
        switch (this.cellType) {
            case CLOSED:
            case OPEN:
            case PATH:
                this.cellType = CellType.DEFAULT;
        }
        setCellStyle();
        hideLabels();
    }

    private void hideLabels() {
        gCost.setVisible(false);
        fCost.setVisible(false);
        hCost.setVisible(false);
    }
}
