package pl.mzap.model;

import javafx.scene.layout.Pane;
import pl.mzap.AppSettings;

public class Grid extends Pane {

    private final AppSettings appSettings = AppSettings.getInstance();

    private Cell[][] cells;
    private Cell startCell;
    private Cell stopCell;

    public Grid(int rows, int columns) {
        cells = new Cell[rows][columns];
    }

    public void addCell(Cell cell) {
        switch (cell.getCellType()) {
            case START:
                startCell = cell;
                break;
            case END:
                stopCell = cell;
                break;
            default:
                this.cells[cell.getRow()][cell.getColumn()] = cell;
                break;
        }
        getChildren().add(cell);
    }

    public void clear() {
        for (int row = 0; row < appSettings.getGridRowSize(); row++) {
            for (int col = 0; col < appSettings.getGridColSize(); col++) {
                cells[row][col].clear();
            }
        }
    }

    public Cell getCell(int row, int column) {
        return cells[row][column];
    }

    public Cell getStartCell() {
        return startCell;
    }

    public Cell getStopCell() {
        return stopCell;
    }
}
