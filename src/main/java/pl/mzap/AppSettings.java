package pl.mzap;

import javafx.geometry.Bounds;

import java.util.Objects;

public class AppSettings {

    private final int gridColSize = 15;
    private final int gridRowSize = 15;

    private double cellXSize;
    private double cellYSize;

    private int printThreadSleep = 10;
    private boolean isDiagonalMove = true;

    private static AppSettings instance;

    public static AppSettings getInstance() {
        if (Objects.isNull(instance)) {
            instance = new AppSettings();
        }
        return instance;
    }

    private AppSettings() {
    }

    public void setCellSize(Bounds parentBounds) {
        this.cellXSize = parentBounds.getWidth() / gridColSize;
        this.cellYSize = parentBounds.getHeight() / gridRowSize;
    }

    public int getGridColSize() {
        return gridColSize;
    }

    public int getGridRowSize() {
        return gridRowSize;
    }

    public double getCellXSize() {
        return cellXSize;
    }

    public double getCellYSize() {
        return cellYSize;
    }

    public int getPrintThreadSleep() {
        return printThreadSleep;
    }

    public void setPrintThreadSleep(int printThreadSleep) {
        this.printThreadSleep = printThreadSleep;
    }

    public boolean isDiagonalMove() {
        return isDiagonalMove;
    }

    public void setDiagonalMove(boolean diagonalMove) {
        isDiagonalMove = diagonalMove;
    }
}
