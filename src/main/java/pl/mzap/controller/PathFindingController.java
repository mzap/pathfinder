package pl.mzap.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import pl.mzap.App;
import pl.mzap.AppSettings;

import java.net.URL;
import java.util.ResourceBundle;

public class PathFindingController implements Initializable {

    private AppSettings appSettings = AppSettings.getInstance();

    public App app;
    public Button findPathBtn;
    public Button cancelPathBtn;
    public Slider printSpeedSlider;
    public Label currentPrintSpeedLabel;
    public ToggleButton diagonalMoveBtn;

    public void setApp(App app) {
        this.app = app;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        printSpeedSlider.setValue(appSettings.getPrintThreadSleep());
        currentPrintSpeedLabel.setText(appSettings.getPrintThreadSleep() + " ms");
        diagonalMoveBtn.setSelected(appSettings.isDiagonalMove());
    }

    @FXML
    public void findPath(ActionEvent actionEvent) {
        System.out.println("Find Path called");
        app.findPath();
    }

    public void cancelPathAction(ActionEvent actionEvent) {
        app.clearBoard();
    }

    public void printSpeedDragAction(MouseEvent mouseEvent) {
        double printSpeed = printSpeedSlider.getValue();
        currentPrintSpeedLabel.setText((int)printSpeed + " ms");
        appSettings.setPrintThreadSleep((int)printSpeed);
    }

    public void diagonalMoveAction(ActionEvent actionEvent) {
        boolean selected = diagonalMoveBtn.isSelected();
        appSettings.setDiagonalMove(selected);
    }
}

