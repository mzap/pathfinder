package pl.mzap.astart;

import java.util.ArrayList;
import java.util.Collection;

public class AStarPathSnapshot {

    private final AStarCell parentNode;
    private final Collection<AStarCell> successors;

    public AStarPathSnapshot(AStarCell parent) {
        this.parentNode = parent.clone();
        this.successors = new ArrayList<>();
    }

    public AStarCell getParentNode() {
        return parentNode;
    }

    public Collection<AStarCell> getSuccessors() {
        return successors;
    }

    public void addSuccessors(Collection<AStarCell> successors) {
        for (AStarCell successor: successors) {
            this.successors.add(successor.clone());
        }
    }

    public void removeSuccessor(AStarCell successor) {
        this.successors.remove(successor);
    }
}
