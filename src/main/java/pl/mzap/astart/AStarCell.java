package pl.mzap.astart;

import pl.mzap.model.Cell;

public class AStarCell implements Cloneable{

    private int row;
    private int column;
    private boolean isObstacle;

    private double gCost;
    private double fCost;
    private double hCost;

    public AStarCell(Cell cell) {
        this.row = cell.getRow();
        this.column = cell.getColumn();
        this.isObstacle = cell.isObstacle();
        this.gCost = 0;
        this.fCost = 0;
        this.hCost = 0;
    }

    public AStarCell(int row, int column, boolean isObstacle) {
        this.row = row;
        this.column = column;
        this.isObstacle = isObstacle;
        this.gCost = 0;
        this.fCost = 0;
        this.hCost = 0;
    }

    public void setGCost(double gCost) {
        this.gCost = gCost;
    }

    public void setFCost(double fCost) {
        this.fCost = fCost;
    }

    public void setHCost(double hCost) {
        this.hCost = hCost;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isObstacle() {
        return isObstacle;
    }

    public double getFCost() {
        return fCost;
    }

    public double getGCost() {
        return gCost;
    }

    public double getHCost() {
        return hCost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj instanceof AStarCell) {
            return obj.hashCode() == this.hashCode();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 1000000 + (this.row * 1000) + (this.column);
    }

    @Override
    protected AStarCell clone() {
        AStarCell clonedCell = new AStarCell(row, column, isObstacle);
        clonedCell.setGCost(gCost);
        clonedCell.setHCost(hCost);
        clonedCell.setFCost(fCost);

        return clonedCell;
    }

    @Override
    public String toString() {
        return "[" + row + "][" + column + "]";
    }
}
