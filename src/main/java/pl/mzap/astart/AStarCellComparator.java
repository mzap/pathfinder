package pl.mzap.astart;

import java.util.Comparator;

public class AStarCellComparator implements Comparator<AStarCell> {
    @Override
    public int compare(AStarCell o1, AStarCell o2) {
        return Double.compare(o1.getFCost(), o2.getFCost());
    }
}
