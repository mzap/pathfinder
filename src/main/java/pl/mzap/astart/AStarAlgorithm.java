package pl.mzap.astart;

import pl.mzap.AppSettings;
import pl.mzap.PathFinding;
import pl.mzap.model.Cell;
import pl.mzap.model.Grid;

import java.util.*;

public class AStarAlgorithm implements PathFinding {

    private AppSettings appSettings = AppSettings.getInstance();

    private AStarCell[][] grid;
    private AStarCell startCell;
    private AStarCell endCell;

    private Collection<AStarPathSnapshot> snapshot = new ArrayList<>();

    public AStarAlgorithm(Grid grid) {
        this.grid = new AStarCell[appSettings.getGridRowSize()][appSettings.getGridColSize()];
        bindGridFromView(grid);
        this.startCell = new AStarCell(grid.getStartCell());
        this.endCell = new AStarCell(grid.getStopCell());
    }

    private void bindGridFromView(Grid grid) {
        for (int row = 0; row < appSettings.getGridRowSize(); row++) {
            for (int column = 0; column < appSettings.getGridColSize(); column++) {
                Cell cell = grid.getCell(row, column);
                AStarCell aStarCell = new AStarCell(cell.getRow(), cell.getColumn(), cell.isObstacle());
                this.grid[row][column] = aStarCell;
            }
        }
    }

    @Override
    public Collection<AStarPathSnapshot> findPath() {
        System.out.println("Iteration Start");
        snapshot.clear();

        Queue<AStarCell> openList = new PriorityQueue<>(new AStarCellComparator());
        List<AStarCell> closedList = new ArrayList<>();
        openList.add(startCell);

        while (!openList.isEmpty()) {
            AStarCell leastAStarCell = openList.poll();
            System.out.println("Get from Open: [" + leastAStarCell.getRow() + "][" + leastAStarCell.getColumn() + "]: " + leastAStarCell.getFCost());

            if (leastAStarCell.equals(endCell)) {
                System.out.println("End cell found [" + leastAStarCell.getRow() + "][" + leastAStarCell.getColumn() + "]");
                break;
            }

            Collection<AStarCell> successors = findSuccessorsFor(leastAStarCell);

            AStarPathSnapshot currentSnapshot = new AStarPathSnapshot(leastAStarCell);
            currentSnapshot.addSuccessors(successors);

            for (AStarCell successor : successors) {
                double successorGCost = leastAStarCell.getGCost() + calculateDistance(successor, leastAStarCell);
                double successorHCost = calculateDistance(successor, endCell);
                double successorFCost = successorGCost + successorHCost;

                Optional<AStarCell> nodeInOpen = findCellInList(successor, openList);
                if (nodeInOpen.isPresent()) {
                    Double nodeGCost = nodeInOpen.map(AStarCell::getGCost).get();
                    if (nodeGCost <= successorGCost) {
                        currentSnapshot.removeSuccessor(successor);
                        continue;
                    }
                }

                Optional<AStarCell> nodeInClosed = findCellInList(successor, closedList);
                if (nodeInClosed.isPresent()) {
                    Double nodeGCost = nodeInClosed.map(AStarCell::getGCost).get();
                    if (nodeGCost <= successorGCost) {
                        currentSnapshot.removeSuccessor(successor);
                        continue;
                    }
                }
                successor.setGCost(successorGCost);
                successor.setHCost(successorHCost);
                successor.setFCost(successorFCost);
                openList.add(successor);
            }
            closedList.add(leastAStarCell);
            this.snapshot.add(currentSnapshot);
        }
        System.out.println("Iteration STOP");
        return snapshot;
    }

    private Optional<AStarCell> findCellInList(AStarCell cell, Collection<AStarCell> list) {
        return list.stream()
                .filter(node -> node.equals(cell))
                .findFirst();
    }

    private double calculateDistance(AStarCell start, AStarCell end) {
        if (appSettings.isDiagonalMove())
            return calculateEuclideanDistance(start, end);
        else return calculateManhattanDistance(start, end);
    }

    private double calculateEuclideanDistance(AStarCell start, AStarCell end) {
        return Math.sqrt(
                Math.abs(start.getColumn() - end.getColumn()) * Math.abs(start.getColumn() - end.getColumn()) +
                        Math.abs((start.getRow() - end.getRow())) * Math.abs((start.getRow() - end.getRow()))
        );
    }

    private double calculateManhattanDistance(AStarCell start, AStarCell end) {
        return Math.abs(start.getColumn() - end.getColumn()) + Math.abs(start.getRow() - end.getRow());
    }


    private Collection<AStarCell> findSuccessorsFor(AStarCell parentCell) {
        int parentColumn = parentCell.getColumn();
        int parentRow = parentCell.getRow();
        Collection<AStarCell> successors = new ArrayList<>();
        getSuccessorByPosition(parentRow, parentColumn + 1).ifPresent(successors::add);
        getSuccessorByPosition(parentRow, parentColumn - 1).ifPresent(successors::add);
        getSuccessorByPosition(parentRow - 1, parentColumn).ifPresent(successors::add);
        getSuccessorByPosition(parentRow + 1, parentColumn).ifPresent(successors::add);

        if (appSettings.isDiagonalMove()) {
            getSuccessorByPosition(parentRow + 1, parentColumn + 1).ifPresent(successors::add);
            getSuccessorByPosition(parentRow + 1, parentColumn - 1).ifPresent(successors::add);
            getSuccessorByPosition(parentRow - 1, parentColumn + 1).ifPresent(successors::add);
            getSuccessorByPosition(parentRow - 1, parentColumn - 1).ifPresent(successors::add);
        }
        return successors;
    }

    private Optional<AStarCell> getSuccessorByPosition(int row, int col) {
        if (isOutOfGrid(row, col) || isObstacle(row, col)) {
            return Optional.empty();
        }
        return Optional.of(grid[row][col]);
    }

    private boolean isObstacle(int row, int column) {
        return grid[row][column].isObstacle();
    }

    private boolean isOutOfGrid(int row, int column) {
        return row < 0 || column < 0 || row >= appSettings.getGridRowSize() || column >= appSettings.getGridColSize();
    }

    @Override
    public String toString() {
        StringBuilder boardBuilder = new StringBuilder();
        boardBuilder.append("[");
        for (int row = 0; row < appSettings.getGridRowSize(); row++) {
            for (int column = 0; column < appSettings.getGridColSize(); column++) {
                boardBuilder.append(grid[row][column].getFCost());
                boardBuilder.append("]");
            }
            boardBuilder.append("\n");
        }
        return boardBuilder.toString();
    }
}
