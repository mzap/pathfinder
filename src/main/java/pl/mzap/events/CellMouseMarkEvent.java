package pl.mzap.events;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import pl.mzap.App;
import pl.mzap.model.Cell;

public class CellMouseMarkEvent {

    private App app;

    EventHandler<MouseEvent> cellOnMousePressed = this::setCellType;

    EventHandler<MouseEvent> cellOnMouseReleased = mouseEvent -> {
        app.findPath();
    };

    EventHandler<MouseEvent> cellOnMouseDragEntered = this::setCellType;

    EventHandler<MouseEvent> cellOnDragDetected = mouseEvent -> {
        Cell enteredCell = (Cell) mouseEvent.getSource();
        enteredCell.startFullDrag();
    };

    public CellMouseMarkEvent(App app) {
        this.app = app;
    }

    private void setCellType(MouseEvent mouseEvent) {
        Cell cell = (Cell) mouseEvent.getSource();
        MouseButton button = mouseEvent.getButton();
        switch (button) {
            case PRIMARY:
                cell.markAsObstacle();
                break;
            case SECONDARY:
                cell.removeObstacle();
                break;
        }
    }


    public void setCellOnMousePressed(Cell cell) {
        cell.setOnMousePressed(cellOnMousePressed);
        cell.setOnMouseReleased(cellOnMouseReleased);
        cell.setOnDragDetected(cellOnDragDetected);
        cell.setOnMouseDragEntered(cellOnMouseDragEntered);
    }

}
