package pl.mzap.events;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import pl.mzap.App;
import pl.mzap.AppSettings;
import pl.mzap.model.Cell;

public class CellMouseDragEvent {

    private AppSettings appSettings = AppSettings.getInstance();
    private App app;

    public CellMouseDragEvent(App app) {
        this.app = app;
    }

    private EventHandler<MouseEvent> onMousePressedEventHandler = mouseEvent -> {
        Cell cell = (Cell) mouseEvent.getSource();
        System.out.println("Pressed" + cell.toString());
    };

    private EventHandler<MouseEvent> onMouseDraggedEventHandler = mouseEvent -> {
        Cell cell = (Cell) mouseEvent.getSource();

        double cellWidth = cell.getWidth();
        double cellHeight = cell.getHeight();

        double sceneXPosition = mouseEvent.getSceneX();
        double sceneYPosition = mouseEvent.getSceneY();

        cell.setLayoutX(sceneXPosition - cellWidth / 2);
        cell.setLayoutY((sceneYPosition - 35) - cellHeight / 2);
    };

    private EventHandler<MouseEvent> onMouseReleasedEventHandle = mouseEvent -> {
        Cell cell = (Cell) mouseEvent.getSource();
        System.out.println("Released" + cell.toString());

        double sceneX = mouseEvent.getSceneX();
        double sceneY = mouseEvent.getSceneY() - 48;


        int col = (int) Math.ceil(sceneX / appSettings.getCellXSize() - 1);
        int row = (int) Math.ceil(sceneY / appSettings.getCellYSize() - 1);
        System.out.println(col + " " + row);
        cell.setPosition(row, col);

        cell.setLayoutX(col * appSettings.getCellXSize());
        cell.setLayoutY(row * appSettings.getCellYSize());
        app.findPath();
    };

    public void setOnDragEvent(Cell cell) {
        cell.addEventFilter(MouseEvent.MOUSE_PRESSED, onMousePressedEventHandler);
        cell.addEventFilter(MouseEvent.MOUSE_DRAGGED, onMouseDraggedEventHandler);
        cell.addEventFilter(MouseEvent.MOUSE_RELEASED, onMouseReleasedEventHandle);
    }

}
