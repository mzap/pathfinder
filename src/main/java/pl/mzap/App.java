package pl.mzap;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pl.mzap.astart.AStarAlgorithm;
import pl.mzap.astart.AStarCell;
import pl.mzap.astart.AStarPathSnapshot;
import pl.mzap.controller.PathFindingController;
import pl.mzap.events.CellMouseDragEvent;
import pl.mzap.events.CellMouseMarkEvent;
import pl.mzap.model.Cell;
import pl.mzap.model.CellType;
import pl.mzap.model.Grid;

import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

public class App extends Application {

    private CellMouseMarkEvent cellMouseMarkEvent;
    private CellMouseDragEvent cellMouseDragEvent;

    private AppSettings appSettings = AppSettings.getInstance();
    private Grid grid;
    private Collection<AStarPathSnapshot> snapshot;
    private PathFindingController controller;
    private Thread printThread;

    @Override
    public void start(Stage stage) throws IOException {
        Parent parent = loadParentFromFXML();
        Scene scene = prepareScene(parent);
        stage.setScene(scene);
        stage.setTitle("Path Finding");
        stage.show();

        BorderPane borderPane = (BorderPane) parent;
        Bounds layoutBounds = borderPane.getCenter().getBoundsInLocal();
        appSettings.setCellSize(layoutBounds);

        cellMouseMarkEvent = new CellMouseMarkEvent(this);
        cellMouseDragEvent = new CellMouseDragEvent(this);

        grid = buildGridView();
        borderPane.setCenter(grid);

        addStartAndStopCellOnGrid();
    }

    private void addStartAndStopCellOnGrid() {
        Cell startCell = new Cell(0, 0, appSettings.getCellXSize(), appSettings.getCellYSize(), CellType.START);
        Cell endCell = new Cell(appSettings.getGridRowSize() - 1, appSettings.getGridColSize() - 1, appSettings.getCellXSize(), appSettings.getCellYSize(), CellType.END);
        cellMouseDragEvent.setOnDragEvent(startCell);
        cellMouseDragEvent.setOnDragEvent(endCell);
        grid.addCell(startCell);
        grid.addCell(endCell);
    }

    private Scene prepareScene(Parent parent) {
        Scene scene = new Scene(parent);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        return scene;
    }

    public void findPath() {
        if (Objects.nonNull(printThread) && printThread.isAlive()) {
            printThread.interrupt();
        }
        findPathAndGetSnapshot();
        Task<Void> printTask = new Task<>() {
            @Override
            protected Void call() throws Exception {
                bindSnapshotToView();
                return null;
            }
        };
        printThread = new Thread(printTask);
        printThread.setDaemon(true);
        printThread.start();
    }

    public void clearBoard() {
        if (Objects.nonNull(printThread) && printThread.isAlive()) {
            printThread.interrupt();
        }
        grid.clear();
    }

    private void findPathAndGetSnapshot() {
        grid.clear();
        AStarAlgorithm aStarAlgorithm = new AStarAlgorithm(grid);
        this.snapshot = aStarAlgorithm.findPath();
    }

    private void bindSnapshotToView() throws InterruptedException {
        for (AStarPathSnapshot currentSnapshot : snapshot) {
            AStarCell parentNode = currentSnapshot.getParentNode();
            grid.getCell(parentNode.getRow(), parentNode.getColumn()).setCellType(CellType.OPEN);
            Platform.runLater(() ->
                    grid.getCell(parentNode.getRow(), parentNode.getColumn()).setCellLabels(parentNode.getGCost(), parentNode.getFCost(), parentNode.getHCost()));
            for (AStarCell aStarCell : currentSnapshot.getSuccessors()) {
                grid.getCell(aStarCell.getRow(), aStarCell.getColumn()).setCellType(CellType.CLOSED);
                Platform.runLater(() ->
                        grid.getCell(aStarCell.getRow(), aStarCell.getColumn()).setCellLabels(aStarCell.getGCost(), aStarCell.getFCost(), aStarCell.getHCost()));
            }
            Thread.sleep(appSettings.getPrintThreadSleep());
        }
    }

    private Grid buildGridView() {
        Grid grid = new Grid(appSettings.getGridRowSize(), appSettings.getGridColSize());
        double cellXSize = appSettings.getCellXSize();
        double cellYSize = appSettings.getCellYSize();

        for (int row = 0; row < appSettings.getGridRowSize(); row++) {
            for (int col = 0; col < appSettings.getGridColSize(); col++) {
                Cell cell = new Cell(row, col, cellXSize, cellYSize);
                cellMouseMarkEvent.setCellOnMousePressed(cell);
                grid.addCell(cell);
            }
        }
        return grid;
    }

    private Parent loadParentFromFXML() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/pathFinding.fxml"));
        Parent parent = fxmlLoader.load();
        controller = fxmlLoader.getController();
        controller.setApp(this);
        return parent;
    }

    public static void main(String[] args) {
        launch();
    }
}